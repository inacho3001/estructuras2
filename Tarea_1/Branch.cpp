//En este archivo se definen los contructores y destructor para los objetos tipo Branch.

#include "Predictors.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>


Branch::Branch(){
            this -> pc = 0;
            this -> result = "E";
}

Branch::Branch(unsigned int pc, char result){
            this -> pc = pc;
            this -> result = result;
}

Branch::Branch(const Branch& orig){
            this -> pc = orig.pc;
            this -> result = orig.result;
}

Branch::~Branch(){

}