# Tarea:Predictores de Saltos

Este programa fue creado como solucion a una tarea del curso Estructuras de Computadoras Digitales II.
Por Ignacio Mora Calderon, carne B64657

# Descripcion

El programa consiste en un simulador para predictores de branches en el cual se obtiene
el PC que realiza el branch, el output real, la prediccion hecha y el resultado de la 
prediccion.

Los predictores comprendidos son:
1. Bimodal Predictor
2. GShare Predictor
3. PShare Predictor
4. Tournament

# Instrucciones
Para compilar ejecutar el makefile con:

make all

Tambien se puede manualmente con:

g++ -std=c++11 -o branch.x main.cpp Branch.cpp GShare.cpp PShare.cpp Result.cpp Functions.cpp -lstdc++ -lm


El programa es parametrizable por lo cual es posible escoger cual se desea correr desde la consola de la siguiente manera:

gunzip -c branch-trace-gcc.trace.gz|branch.x -s <#> -bp <#> -gh <#> -ph <#> -o <#>

Donde los paremetros representan:

1.  Tamano de la tabla BTH. (-s): Numero de bits al final de PC que se utilizaran para indexar.

2.  Tipo de prediccion (-bp):   0 -> Bimodal
                                1 -> GShare
                                2 -> PShare
                                3 -> Tournament

3.  Tamano del registro de prediccion global (-gh): Numero de bits de historia para el predictor GShare

4.  Tamano de los registros de historia privada (-ph): Numero de bits de historia para el predictor PShare.

5.  Salida de la simulacion (-o): Si es 1 el programa escribe un archivo de texto con los 5000 primeros resultados.

