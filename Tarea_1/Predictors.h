//Este es el header que contiene las declaraciones de todas las clases y funciones
//que se utilizaran para desarrollar la tarea.

#ifndef PREDICTORES
#define PREDICTORES

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

//Clase para crear objeto tipo Branch en el cual se almacena la direccion del PC
//junto con el output verdadero al tomar ese branch.
class Branch{
    public:
        Branch();
        Branch(unsigned int pc, char result);
        Branch(const Branch& orig);
        ~Branch();
        
        unsigned long pc; //variable para el PC.
        string result; //variable para el output verdadero.
};

//Clase para crear objeto tipo GShare en el cual se almacena un array que representa la bht
//del predictor, un variable para representar los bits de historia y por ultimo la cantidad 
//de bits de historia que se usaran en la predicccion.
class GShare{
    public:
        GShare();
        GShare(vector<unsigned int> bht, unsigned int history, int history_bits);
        GShare(const GShare& orig);
        ~GShare();

        vector<unsigned int> bht; //variable para bht.
        unsigned int history; //variable para bits de historia.
        int history_bits; //variable para numero de bits a utilizar.
};

//Clase para crear objeto tipo PShare en el cual se almacena un array que representa la bht
//del predictor, un variable para representar los bits de historia que en este caso es otro array 
//que representa la pht y por ultimo la cantidad de bits de historia que se usaran en la predicccion.
class PShare{
    public:
        PShare();
        PShare(vector<unsigned int> bht, vector<unsigned int> history, int history_bits);
        PShare(const PShare& orig);
        ~PShare();

        vector<unsigned int> bht; //variable para bht.
        vector<unsigned int> history; //variable para pht.
        int history_bits; //variable para numero de bits a utilizar.
};

//Clase para crear objeto tipo Result en el cual se almacenan los resultados de la prediccion
//los cuales son PC, outcome real, prediccion y correcto/incorrecto.
class Result{
    public:
        Result();
        Result(unsigned long pc, string outcome, string prediction, string result);
        Result(const Result& orig);
        ~Result();

        unsigned long pc; //variable para PC.
        string outcome; //variable para outcome real.
        string prediction; //variable para prediccion.
        string result; //variable para correcto/incorrecto.
};

//Estas son las declaraciones para las funciones, su funcionamiento asi como
//las variables de entrada y salida se explican en las implementaciones.
vector<Branch> read();
unsigned long bit_mask(int n);
vector<unsigned int> bits_table(int s, unsigned int value);
vector<Result> r_bimodal(int s, vector<Branch>& branch, vector<unsigned int> counter, int* ct, int* it, int* cn, int* in);
vector<Result> r_gshare(int s, vector<Branch>& branch, GShare gshare, int* ct, int* it, int* cn, int* in);
vector<Result> r_pshare(int s, vector<Branch>& branch, PShare pshare, int* ct, int* it, int* cn, int* in);
vector<Result> r_tournament(int s, vector<Branch>& branch, vector<Result>& gshare, vector<Result>& pshare, vector<unsigned int> meta, int* ct, int* it, int* cn, int* in);
void write(vector<Result>& r, string name);
void print(string predictor, int BHT_size, int gshare_size, int pshare_size, int n_branches, int ct, int it, int cn, int in);

#endif