//En este archivo se definen los contructores y destructor para los objetos tipo PShare.

#include "Predictors.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>


PShare::PShare(){

}

PShare::PShare(vector<unsigned int> bht, vector<unsigned int> history, int history_bits){
    this -> bht = bht;
    this -> history = history;
    this -> history_bits = history_bits;
}

PShare::PShare(const PShare& orig){
    this -> bht = orig.bht;
    this -> history = orig.history;
    this -> history_bits = orig.history_bits;
}

PShare::~PShare(){

}