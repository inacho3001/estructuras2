#include "Predictors.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

int main(int argc, char const *argv[]){
    // se toman los valores de los parametro pasados por la consola al ejecutar el programa.
    int s = stoi(argv[2]);
    int bp = stoi(argv[4]);
    int gh = stoi(argv[6]);
    int ph = stoi(argv[8]);
    int o = stoi(argv[10]);
    // se crean todas las variables para ejecutar el programa
    vector<Branch> mem = read();
    vector<unsigned int> bht = bits_table(s, 0b00);
    vector<unsigned int> pht = bits_table(s, 0);
    vector<unsigned int> meta = bits_table(s, 0b11);
    int ct = 0;
    int it = 0;
    int cn = 0;
    int in = 0;

    //Dependiendo del valor de bp se ejecuta el predictor respectivo.

    //Bimodal
    if(bp == 0){
        string predictor = "Bimodal";
        vector<Result> result_bi = r_bimodal(s, mem, bht, &ct, &it, &cn, & in);
        if(o == 1){
            write(result_bi,"bimodal.txt");
        }
        print(predictor, bht.size(), gh, ph, result_bi.size(), ct, it, cn, in);
    }
    //GShare
    if(bp == 1){
        string predictor = "GShare";
        GShare gshare(bht, 0, gh);
        vector<Result> result_g = r_gshare(s, mem, gshare, &ct, &it, &cn, & in);
        if(o == 1){
            write(result_g,"gshare.txt");
        }
        print(predictor, bht.size(), gh, ph, result_g.size(), ct, it, cn, in);
    }

    //PShare
    if(bp == 2){
        string predictor = "PShare";
        PShare pshare(bht, pht, ph);
        vector<Result> result_p = r_pshare(s, mem, pshare, &ct, &it, &cn, & in);
        if(o == 1){
            write(result_p,"pshare.txt");
        }
        print(predictor, bht.size(), gh, ph, result_p.size(), ct, it, cn, in);
    }

    //Tournament
    if(bp == 3){
        string predictor = "Tournament";
        GShare gshare(bht, 0, gh);
        PShare pshare(bht, pht, ph);
        vector<Result> result_g = r_gshare(s, mem, gshare, &ct, &it, &cn, & in);
        vector<Result> result_p = r_pshare(s, mem, pshare, &ct, &it, &cn, & in);
        ct = 0;
        it = 0;
        cn = 0;
        in = 0;
        vector<Result> result_t = r_tournament(s, mem, result_g, result_p , meta, &ct, &it, &cn, & in);
        if(o == 1){
            write(result_t,"tournament.txt");
        }
        print(predictor, bht.size(), gh, ph, result_t.size(), ct, it, cn, in);
    }

    return 0;
}
