//En este archivo se definen los contructores y destructor para los objetos tipo GShare.

#include "Predictors.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>


GShare::GShare(){

}

GShare::GShare(vector<unsigned int> bht, unsigned int history, int history_bits){
    this -> bht = bht;
    this -> history = history;
    this -> history_bits = history_bits;
}

GShare::GShare(const GShare& orig){
    this -> bht = orig.bht;
    this -> history = orig.history;
    this -> history_bits = orig.history_bits;
}

GShare::~GShare(){

};