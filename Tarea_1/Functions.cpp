//En este archivo se definen todas las implementaciones de las funciones del Predictor.h

#include "Predictors.h"
#include <iostream>
#include <fstream>
#include <vector> //se utilza el template vector de STL de C++ para arrays dinamicos.
#include <string>
#include <cmath>

using namespace std;


//Funcion read
//Descripcion: Funcion para leer los datos del archivo de texto comprimido con los datos
//para realizar las simulaciones, los cuales son contenidos en un array de objetos 
//tipo Branch.
//parametros de entrada: ninguna.
//parametros de salida: array dinamico tipo vector.
vector<Branch> read(){
    vector<Branch> result;
    string aux; 
    Branch *actual = new Branch();
    while (!cin.eof()){
        if(actual->pc == 0 || actual->result == "E"){
            cin >> aux; //se obtiene los datos del standar input y se introducen en una varible auxiliar para introducirlo en el objeto tipo Branch.
            if(aux == "T" || aux =="N"){
                actual->result = aux;
            }
            else{
                actual->pc = stoul(aux,nullptr,10); //se convierte una string para obtener el valor en decimal del PC.
            }
        }
        else{
            result.push_back(*(actual)); //se agrega una nueva entrada al array dinamico con los nuevos datos de PC y outcome.
            delete actual;
            actual = new Branch();
        }
    }
    delete actual;
    return result;
}

//Funcion write
//Descripcion: Funcion para escribir los resultados de un predictor solicitado cuando
//la opcion -o es 1 en un archivo de texto
//parametros de entrada: array con los resultados a guardar y nombre del archivo a crear.
//parametros de salida: ninguno.
void write(vector<Result>& r, string name){
    ofstream file;
    file.open(name);
    if(file.is_open()){
        file << "PC" << "          " << "Outcome" << "  " << "Prediction" << "  " << "correct/incorrect" << endl;
        for(int i = 0; i < 5000; i++){ //para cada entrada se escribe linea por linea en el archivo de texto usando un stream de datos.
            file << r[i].pc << "  " << r[i].outcome << "        " << r[i].prediction << "           " << r[i].result << endl;
        }
        file.close();
    }
    else cout << "Unable to write file";
}

//Funcion print
//Descripcion: Funcion para imprimir los datos solicitados en la tarea en la consola.
//parametros de entrada: Nombre del predictos utilizadp, tamano de BHT, tamano del
//registro de historia para GShare, tamano del registro de historia para PShare,
//numero total de branches, # de taken correctos, # de taken incorrectos, # de not taken
//correctos y # de not taken incorrectos.
//parametros de salida: ninguno.
void print(string predictor, int bht_size, int gshare_size, int pshare_size, int n_branches, int ct, int it, int cn, int in){
    double percent = (((double)ct+(double)cn)*100)/(double)n_branches;
    cout << "-------------------------------------------------------------"<< endl;
    cout << "Prediction parameters: " << endl;
    cout << "-------------------------------------------------------------"<< endl;
    cout << "Branch prediction type:" << "                            " << predictor << endl;
    cout << "BHT size (entries):" << "                                " << bht_size << endl;
    cout << "Global history register size:" << "                      " << gshare_size << endl;
    cout << "Private history register size:" << "                     " << pshare_size << endl;
    cout << "-------------------------------------------------------------"<< endl;
    cout << "Simulation Result: " << endl;
    cout << "-------------------------------------------------------------"<< endl;
    cout << "Number of branch:" << "                                  " << n_branches << endl;
    cout << "Number of correct prediction of taken branches:" << "    " << ct << endl;
    cout << "Number of incorrect prediction of taken branches:" << "  " << it << endl;
    cout << "Correct prediction of not taken branches:" << "          " << cn << endl;
    cout << "Incorrect prediction of not taken branches:" << "        " << in << endl;
    cout << "Percentage of correct predictions:" << "                 " << percent << endl;
    cout << "-------------------------------------------------------------"<< endl;
}

//Funcion bit_mask
//Descripcion: Funcion para crear una mascara de bits para obener los n ultimos bits.
//parametros de entrada: numero n de bits de la mascara.
//parametros de salida: n bits para obtener los ultimos n bits de otro numero.
unsigned long bit_mask(int n){
    unsigned long mask = 0;
    if(n==32){
        mask = 4294967295; //numero que representan 32 1's en binario.
    }
    else{
        for (int i = 0; i < n; i ++) {
            mask = mask | (1 << i); // se realiza la operacion or para obtener un numero con n 1's.
        }   
    }
    return mask;
}

//Funcion bit_table
//Descripcion: Funcion para crear un array que simula una table con numeros binarios.
//parametros de entrada: parametro s que se pasa al programa a la consola que representa
//los ultimos n bits que se quieren obtener del PC y valor inicial en bits que tendra cada
//entrada en la tabla.
//parametros de salida: array con valores binarios en cada entrada.
vector<unsigned int> bits_table(int s, unsigned int value){
    int size = pow(2,s); //tamano de la tabla es 2^s, se puede utilizar para BHT,PHT o tabla de metapredictor.
    unsigned int actual = value;
    vector<unsigned int> result;
    for(int i = 0; i < size; i++){
        result.push_back(actual);
    }
    return result;
}
//Funcion r_bimodal
//Descripcion: Funcion para obtener los resultados de las predicciones generadas por el
//predictor bimodal, estos son almacenados en un arreglo de objetos tipo Result.
//parametro de entrada: # s de bits del PC, un array de tipo Branch, una tabla BHT de contadores binarios, 
//punteros a ints donde se almacenan # de taken correctos, # de taken incorrectos, # de not taken
//correctos y # de not taken incorrectos.
//parametros de salida: array de obejetos tipo Result.
vector<Result> r_bimodal(int s, vector<Branch>& branch, vector<unsigned int> counter, int* ct, int* it, int* cn, int* in){
    vector<Result> result;
    unsigned int ST = 0b11; //definicion de los estados Strong taken, Weakly taken, Weakly not taken y Strong not taken.
    unsigned int WT = 0b10;
    unsigned int WN = 0b01;
    unsigned int SN = 0b00;
    unsigned long mask = bit_mask(s);
    unsigned int bits;
    string guess;
    for(int eval = 0; eval < branch.size(); eval++){ //Ciclo para evaluar todas los valores de PC con branch.
        bool decision = false; // variable para saber que se tomo una decision respecto a la prediccion
        bits = branch[eval].pc & mask;
        if(counter[bits] == SN && decision == false){ //en cada if se evalua cual sera la prediccion asi como el cambio en el contador dependiendo del valor actual.
            guess = "N";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                counter[bits] = WN;
                decision = true;
            }
        }
        if(counter[bits] == WN && decision == false){
            guess = "N";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                counter[bits] = SN;
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                counter[bits] = WT;
                decision = true;
            }
        }
        if(counter[bits] == WT && decision == false){
            guess = "T";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                counter[bits] = ST;
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                counter[bits] = WN;
                decision = true;
            }
        }
        if(counter[bits] == ST && decision == false){
            guess = "T";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                counter[bits] = WT;
                decision = true;
            }
        }
        // bloque de codigo para incrementar los contadores de # de taken correctos, # de taken incorrectos, # de not taken
        //correctos y # de not taken incorrectos.
        if(result[eval].result == "correct" && result[eval].outcome == "T"){
            *(ct) = *(ct)+1 ;
        }
        if(result[eval].result == "incorrect" && result[eval].outcome == "T"){
            *(it) = *(it)+1;
        }
        if(result[eval].result == "correct" && result[eval].outcome == "N"){
            *(cn) = *(cn)+1;
        }
        if(result[eval].result == "incorrect" && result[eval].outcome == "N"){
            *(in) = *(in)+1;
        }
    }
    return result;
}
//A partir las implementaciones del GShare y PShare son muy similares por lo que se comentan solo los cambios.

//Funcion r_gshare
//Descripcion: Funcion para obtener los resultados de las predicciones generadas por el
//predictor de historia global, estos son almacenados en un arreglo de objetos tipo Result.
//parametro de entrada: # s de bits del PC, un array de tipo Branch, un objeto tipo GShare y 
//punteros a ints donde se almacenan # de taken correctos, # de taken incorrectos, # de not taken
//correctos y # de not taken incorrectos.
//parametros de salida: array de obejetos tipo Result.
vector<Result> r_gshare(int s, vector<Branch>& branch, GShare gshare, int* ct, int* it, int* cn, int* in){
    vector<Result> result;
    unsigned int ST = 0b11;
    unsigned int WT = 0b10;
    unsigned int WN = 0b01;
    unsigned int SN = 0b00;
    unsigned long pc_mask = bit_mask(s);
    unsigned long hist_mask = bit_mask(gshare.history_bits); // variable para crear una mascar y tener siempre el mismo numero de bits de historia.
    unsigned int pc_bits;
    unsigned int hist_bits;
    unsigned int bits;
    string guess;
    for(int eval = 0; eval < branch.size(); eval++){
        bool decision = false;
        pc_bits = branch[eval].pc & pc_mask;
        hist_bits = gshare.history;
        bits = (pc_bits ^ hist_bits); //se realiza la operacion XOR entre los bits de PC y los de historia para indexar al BHT.
        bits = bits & pc_mask; //el numero de bits que se toma de la operacion anterior es igual al numero de bits desde el PC.
        if(gshare.bht[bits] == SN && decision == false){
            guess = "N";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                gshare.bht[bits] = WN;
                decision = true;
            }
        }
        if(gshare.bht[bits] == WN && decision == false){
            guess = "N";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                gshare.bht[bits] = SN;
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                gshare.bht[bits] = WT;
                decision = true;
            }
        }
        if(gshare.bht[bits] == WT && decision == false){
            guess = "T";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                gshare.bht[bits] = ST;
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                gshare.bht[bits] = WN;
                decision = true;
            }
        }
        if(gshare.bht[bits] == ST && decision == false){
            guess = "T";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                gshare.bht[bits] = WT;
                decision = true;
            }
        }
        // if para actualizar los bits de historia de esa entrada en la BHT.
        // se utiliza una mascara para no salirse del rango de bits establecido.
        if(branch[eval].result == "T"){
            gshare.history = (1 << 0) | (gshare.history << 1);
            gshare.history = gshare.history & hist_mask;
        }
        else
        {
            gshare.history = gshare.history << 1;
            gshare.history = gshare.history & hist_mask;
        }
        if(result[eval].result == "correct" && result[eval].outcome == "T"){
            *(ct) = *(ct)+1 ;
        }
        if(result[eval].result == "incorrect" && result[eval].outcome == "T"){
            *(it) = *(it)+1;
        }
        if(result[eval].result == "correct" && result[eval].outcome == "N"){
            *(cn) = *(cn)+1;
        }
        if(result[eval].result == "incorrect" && result[eval].outcome == "N"){
            *(in) = *(in)+1;
        }
    }
    return result;
}

//Funcion r_pshare
//Descripcion: Funcion para obtener los resultados de las predicciones generadas por el
//predictor de historia global, estos son almacenados en un arreglo de objetos tipo Result.
//parametro de entrada: # s de bits del PC, un array de tipo Branch, un objeto tipo PShare y 
//punteros a ints donde se almacenan # de taken correctos, # de taken incorrectos, # de not taken
//correctos y # de not taken incorrectos.
//parametros de salida: array de obejetos tipo Result.
vector<Result> r_pshare(int s, vector<Branch>& branch, PShare pshare, int* ct, int* it, int* cn, int* in){
    vector<Result> result;
    unsigned int ST = 0b11;
    unsigned int WT = 0b10;
    unsigned int WN = 0b01;
    unsigned int SN = 0b00;
    unsigned long pc_mask = bit_mask(s);
    unsigned long hist_mask = bit_mask(pshare.history_bits);
    unsigned int pc_bits;
    unsigned int hist_bits;
    unsigned int bits;
    string guess;
    for(int eval = 0; eval < branch.size(); eval++){
        bool decision = false;
        pc_bits = branch[eval].pc & pc_mask;
        hist_bits = pshare.history[pc_bits]; //el codigo es muy parecido al anterior solo que los bits de historia se obtienen de una entrada de la PHT escogidos con los bits de PC.
        bits = (pc_bits ^ hist_bits); 
        bits = bits & pc_mask;
        if(pshare.bht[bits] == SN && decision == false){
            guess = "N";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                pshare.bht[bits] = WN;
                decision = true;
            }
        }
        if(pshare.bht[bits] == WN && decision == false){
            guess = "N";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                pshare.bht[bits] = SN;
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                pshare.bht[bits] = WT;
                decision = true;
            }
        }
        if(pshare.bht[bits] == WT && decision == false){
            guess = "T";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                pshare.bht[bits] = ST;
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                pshare.bht[bits] = WN;
                decision = true;
            }
        }
        if(pshare.bht[bits] == ST && decision == false){
            guess = "T";
            if(guess ==  branch[eval].result){
                Result r(branch[eval].pc, branch[eval].result, guess, "correct");
                result.push_back(r);
                decision = true;
            }
            else
            { 
                Result r(branch[eval].pc, branch[eval].result, guess, "incorrect");
                result.push_back(r);
                pshare.bht[bits] = WT;
                decision = true;
            }
        }
        if(branch[eval].result == "T"){
            pshare.history[pc_bits] = (1 << 0) | (pshare.history[pc_bits] << 1);
            pshare.history[pc_bits] = pshare.history[pc_bits] & hist_mask;
        }
        else
        {
            pshare.history[pc_bits] = pshare.history[pc_bits] << 1;
            pshare.history[pc_bits] = pshare.history[pc_bits] & hist_mask;
        }
        if(result[eval].result == "correct" && result[eval].outcome == "T"){
            *(ct) = *(ct)+1 ;
        }
        if(result[eval].result == "incorrect" && result[eval].outcome == "T"){
            *(it) = *(it)+1;
        }
        if(result[eval].result == "correct" && result[eval].outcome == "N"){
            *(cn) = *(cn)+1;
        }
        if(result[eval].result == "incorrect" && result[eval].outcome == "N"){
            *(in) = *(in)+1;
        }    
    }
    return result;
}

//Funcion r_tournament
//Descripcion: Funcion para obtener los resultados de las predicciones generadas por el
//predictor de torneo, estos son almacenados en un arreglo de objetos tipo Result.
//parametro de entrada: # s de bits del PC, un array de tipo Branch, un array con los resultados
//de los predictores GShare y PShare, un array de bits que simboliza la tabla para el metapredictor y 
//punteros a ints donde se almacenan # de taken correctos, # de taken incorrectos, # de not taken
//correctos y # de not taken incorrectos.
//parametros de salida: array de obejetos tipo Result.
vector<Result> r_tournament(int s, vector<Branch>& branch, vector<Result>& gshare, vector<Result>& pshare, vector<unsigned int> meta,int* ct, int* it, int* cn, int* in){
    vector<Result> result;
    unsigned int SPPshare = 0b11; // Se definen los estados para el predictor siendo estos:
    unsigned int WPPshare = 0b10; // 11 para Strongly Preferred PShare y yendo hacia abajo 
    unsigned int WPGshare = 0b01; // hasta llegar a 00 Strongly Preferred GShare
    unsigned int SPGshare = 0b00;
    unsigned long pc_mask = bit_mask(s);
    unsigned int pc_bits;
    for(int eval = 0; eval < branch.size(); eval++){
        bool decision = false;
        pc_bits = branch[eval].pc & pc_mask;
        if(meta[pc_bits] == SPPshare && decision == false){ //Dependiendo del estado de la entrada de la tabla para el predictor se escoge entre la prediccion del GShare o PShare.
            Result r(pshare[eval].pc, pshare[eval].outcome, pshare[eval].prediction, pshare[eval].result);
            result.push_back(r);
            if(gshare[eval].result == "correct" && pshare[eval].result == "incorrect"){
                meta[pc_bits] = WPPshare;
            }
            decision = true;
        }
        if(meta[pc_bits] == WPPshare && decision == false){
            Result r(pshare[eval].pc, pshare[eval].outcome, pshare[eval].prediction, pshare[eval].result);
            result.push_back(r);
            if(gshare[eval].result == "correct" && pshare[eval].result == "incorrect"){
                meta[pc_bits] = WPGshare;
            }
            if(gshare[eval].result == "incorrect" && pshare[eval].result == "correct"){
                meta[pc_bits] = SPPshare;
            }
            decision = true;
        }
        if(meta[pc_bits] == WPGshare && decision == false){
            Result r(gshare[eval].pc, gshare[eval].outcome, gshare[eval].prediction, gshare[eval].result);
            result.push_back(r);
            if(gshare[eval].result == "correct" && pshare[eval].result == "incorrect"){
                meta[pc_bits] = SPGshare;
            }
            if(gshare[eval].result == "incorrect" && pshare[eval].result == "correct"){
                meta[pc_bits] = WPPshare;
            }
            decision = true;
        }
        if(meta[pc_bits] == SPGshare && decision == false){
            Result r(gshare[eval].pc, gshare[eval].outcome, gshare[eval].prediction, gshare[eval].result);
            result.push_back(r);
            if(gshare[eval].result == "incorrect" && pshare[eval].result == "correct"){
                meta[pc_bits] = WPGshare;
            }
            decision = true;
        }
        if(result[eval].result == "correct" && result[eval].outcome == "T"){
            *(ct) = *(ct)+1 ;
        }
        if(result[eval].result == "incorrect" && result[eval].outcome == "T"){
            *(it) = *(it)+1;
        }
        if(result[eval].result == "correct" && result[eval].outcome == "N"){
            *(cn) = *(cn)+1;
        }
        if(result[eval].result == "incorrect" && result[eval].outcome == "N"){
            *(in) = *(in)+1;
        }
    }
    return result;
}