//En este archivo se definen los contructores y destructor para los objetos tipo Result.

#include "Predictors.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>


Result::Result(){

}

Result::Result(unsigned long pc, string outcome, string prediction, string result){
    this -> pc = pc;
    this -> outcome = outcome;
    this -> prediction = prediction;
    this -> result = result;
}

Result::Result(const Result& orig){
    this -> pc = orig.pc;
    this -> outcome = orig.outcome;
    this -> prediction = orig.prediction;
    this -> result = orig.result;
}

Result::~Result(){

}